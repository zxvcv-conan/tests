
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "mock_fifo.hpp"

using::testing::_;

extern "C"
{
    #include "tests.h"
}

class Tests_UT: public ::testing::Test {
public:
    Tests_UT(){}
    ~Tests_UT(){}

    virtual void SetUp() {}
    virtual void TearDown() {}
};

/************************** TESTS **************************/

TEST_F(Tests_UT, test_add)
{
    Mock_fifo mock;

    EXPECT_CALL(mock, fifo_create(_)).Times(1);
    int result = add(2, 3);

    EXPECT_EQ(result, 5);
}

TEST_F(Tests_UT, test_decision)
{
    int result = decision(2, 3, -1);
    EXPECT_EQ(result, 3);
}
