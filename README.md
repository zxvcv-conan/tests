Tests component
===============

[test link](docs/test.html)

This is a test dummy conan component.

```
sudo service docker start
python3 -m zxvcv.conan-toolbox.multibuild alfa -u zxvcv --pure-c
```

TODO:
=====
- running just UT in docker for specific environment
- fix bug with building test_package inside source directory
- automatic mock generation
- bugfix: WARNING: GCC OLD ABI COMPATIBILITY


```
https://docs.conan.io/en/latest/reference/commands/development/build.html
```


Building step by step
=====================
Create build folder:
```
mkdir bld && cd bld
```

Get source code in the src directory:
```
conan source ..
```

Install requirements, and generate info files:
```
conan install ..
```

Build package:
```
conan build ..
```

To run build stages separately, use (it's not working for some reason):
```
conan build .. --configure
conan build .. --build
conan build .. --install
conan build .. --test
```

Run build commands in docker
============================
Run docker image:
```
docker run -d -t -v ${HOME}/.conan:/root/.conan -v $(pwd):/home/conan/pckg --name conan_container2 conanio/gcc10
```
Or in interactive mode:
```
docker run -ti -v ${HOME}/.conan:/root/.conan -v $(pwd):/home/conan/pckg conanio/gcc10
```

Create package inside docker image:
```
docker exec conan_container2 bash -c "cd /home/conan/pckg && conan create . zxvcv/docker --profile=profiles/linux--arm_m4.cfg"
```

```
docker run -ti -v ${HOME}/.conan:/root/.conan -v $(pwd):/home/conan/pckg gcc-arm-none-eabi:latest
conan create . zxvcv/docker --profile=profiles/linux--arm_m4.cfg
```

```
docker run -ti -w=/home/conan/pckg -v ${HOME}/.conan:/root/.conan -v $(pwd):/home/conan/pckg zxvcv/gcc-arm-none-eabi:latest bash -c "conan create . zxvcv/docker --profile=profiles/linux--arm_m4.cfg"
```

```
docker run -ti -w=/home/conan/pckg -v ${HOME}/.conan:/root/.conan -v $(pwd):/home/conan/pckg zxvcv/gcc:latest bash -c "conan create . zxvcv/docker --profile=profiles/linux--linux_shared.cfg"
```

```
docker run -ti -w=/home/conan/pckg -v ${HOME}/.conan:/root/.conan -v $(pwd):/home/conan/pckg zxvcv/gcc:latest bash -c "conan create . zxvcv/docker --profile=profiles/linux--linux_static.cfg"
```

Compile Package
===============
This way is not recommended, because all parameters needs to be passed from comandline.
Use conan instead.

inside root project directory:
```
mkdir bld && cd bld
cmake .. -DCMAKE_BUILD_TYPE=Release
cmake --build .
```

Compile and Run UT
==================
inside root ut project directory:
```
mkdir bld && cd bld
cmake .. -DCMAKE_BUILD_TYPE=Debug
cmake --build . --target tests_ut
ctest
```

Uploading packages to Remote
============================
```
conan create . zxvcv-conan+tests/testing --profile=profiles/linux_test.cfg
conan upload tests/0.2.1@zxvcv-conan+tests/testing --all -r=gitlab
```

