from conan import ConanFile
from conans.errors import ConanInvalidConfiguration
from conan.tools.cmake import CMakeToolchain, CMake, cmake_layout


class ConanPackage(ConanFile):
    name = "tests"
    version = "0.2.5"

    # Optional metadata
    license = "Eclipse Public License - v 2.0"
    author = "Pawel Piskorz ppiskorz0@gmail.com"
    url = "https://gitlab.com/zxvcv-conan/tests"
    description = """ Test conan package."""
    topics = ("tests", "dummy")

    # Binary configuration
    settings = "os", "compiler", "build_type", "arch", "fpu"
    options = {
        "shared": [True, False]
    }
    default_options = {
        "shared": False
    }
    requires = (
        "fifo/[<1.0.0]@zxvcv-conan+fifo/testing"
    )

    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = "CMakeLists.txt", "src/*", "include/*", "unittest/*"

    def configure(self):
        # this is a C library, and does not depend on any C++ standard library
        del self.settings.compiler.libcxx
        del self.settings.compiler.cppstd

        # if not embedded
        if self.settings.arch not in ["cortex-m4", "avr"]:
            del self.settings.fpu

    def validate(self):
        # embedded
        if self.settings.os == "baremetal":
            # compiler restrictions
            if self.settings.arch == "avr" and not self.settings.compiler == "avr-gcc":
                raise ConanInvalidConfiguration("AVR microcontrollers architecture supports only avr-gcc compiler.")
            if self.settings.arch in ["cortex-m4"] and not self.settings.compiler == "arm-none-eabi-gcc":
                raise ConanInvalidConfiguration("ARM microcontrollers architecture supports only arm-none-eabi-gcc compiler.")

            if self.options.shared:
                raise ConanInvalidConfiguration("Shared library not supprted on embedded systems.")

    def layout(self):
        cmake_layout(self)

    def generate(self):
        tc = CMakeToolchain(self)
        tc.variables["CMAKE_C_FLAGS"] = " ".join(self._get_c_compilation_flags())
        tc.variables["CMAKE_CXX_FLAGS"] = " ".join(self._get_cpp_compilation_flags())
        tc.generate()

    def build(self):
        cmake = CMake(self)
        if self.should_configure:   cmake.configure()
        if self.should_build:       cmake.build()
        # if self.should_test:        cmake.test()

    def package(self):
        self.copy("LICENSE", dst="licenses", src=".")
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.libs = ["tests"]
        self.cpp_info.defines = [] # preprocessor defines here
        self.cpp_info.cflags = self._get_c_compilation_flags()  # pure C flags
        self.cpp_info.cxxflags = self._get_cpp_compilation_flags()  # C++ compilation flags
        # self.cpp_info.sharedlinkflags = []  # linker flags
        # self.cpp_info.exelinkflags = [] # linker flags (executables)
        # self.cpp_info.requires = None # TODO[PP]: check if there will be requires filled with proper way

        self.env_info.CC = self.env["CC"] if "CC" in self.env else None
        self.env_info.CXX = self.env["CXX"] if "CXX" in self.env else None

    def imports(self):
        self.copy("*.h")

    def _get_c_compilation_flags(self):
        flags = []
        if self.settings.arch in ["cortex-m4"]:
            flags = [
                # nano.specs defines the system include path and library parameters to use newlib-nano.
                "-specs=nano.specs",
                # defines that system calls should be implemented as stubs that return errors when called (-lnosys)
                "-specs=nosys.specs",
                # select name of the target ARM processor
                "-mcpu=cortex-m4",
                # set support for Thumb instruction sets
                "-mthumb",
                # specify what floating-point hardware is available on target
                "-mfpu=fpv4-sp-d16",
                # specify floating-point ABI - allows generation of floating-point instructions and uses FPU-specific calling conventions
                "-mfloat-abi=hard"
            ]
        return flags

    def _get_cpp_compilation_flags(self):
        return self._get_c_compilation_flags()
