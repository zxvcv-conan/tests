Changelog
=========

0.2.5 (2022-10-18)
------------------
- Requires user update.
- Docs test html file.

0.2.4 (2022-09-26)
------------------
- unittests/CMakeLists.txt uses cmake util lib
- cleanups

0.2.3 (2022-09-24)
------------------
- Multi package build preparations.

0.2.2 (2022-09-16)
------------------
- Cleanups.
- Add verification() in conanfile.py.
- New settings - fpu.
- Updates package_info().
- Removed mock subdirectory (mocks are generated automatically).
- Updates in profiles.

0.2.1 (2022-09-14)
------------------
- Unit tests updates.
- Build system updates.
- Mocks are generated fully automated.

0.2.0 (2022-08-15)
------------------
- Bacis multibuilder added.
- Building automation config.
- Build steps update in conanfile.py

0.1.0 (2022-08-06)
------------------
- Initial test package.
